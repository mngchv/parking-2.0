import java.util.Random;
import java.util.Scanner;



public class ParkingLotManager {
    private final boolean isOn;
    public ParkingLotManager() {
        this.isOn = true;
    }

    public boolean isOn() {
        return isOn;
    }


    public char Instructions() {
        System.out.println("Commands:");
        System.out.println(" ");
        System.out.println("continue");
        System.out.println("stop");
        System.out.println("status");
        System.out.println("clear ");

        return 0;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of car slots: ");
        int carSlots = scanner.nextInt();
        Parking carParking = new Parking(carSlots, "car");
        System.out.println("Enter the number of truck slots: ");
        int truckSlots = scanner.nextInt();
        Parking truckParking = new Parking(truckSlots, "truck");
        ParkingLotManager PLM = new ParkingLotManager();
        System.out.println(PLM.Instructions());
        Random random = new Random();
        int id = 1000;
        System.out.println("Enter Command:");
        while(PLM.isOn()){
            carParking.updateStatus();
            truckParking.updateStatus();
            switch(scanner.nextLine()){

                case("continue"):{
                    int carAmount = random.nextInt(carSlots/2 + 1);
                    while(carAmount > 0){
                        Car car = new Car();
                        if(carParking.getEmptySlots() > 0) {
                            int steps = random.nextInt(10) + 1;
                            int k = carParking.returnEmptySlot();
                            carParking.park(car, steps, k);
                            System.out.println("Car with id " + id + " in " + car.getSlot() + "slot for " + car.getTimeForCar() + " steps");
                        }
                        else{
                            System.out.println("Sorry, but there are no empty parking places for cars");
                            System.out.println("You should wait " + carParking.getWhenSlotsBeEmpty() + " steps until the parking will be available");
                            break;
                        }
                        carAmount--;
                        carParking.updateStatus();
                        id++;
                    }
                    int truckAmount = random.nextInt(truckSlots/2+1);
                    while(truckAmount > 0){
                        Car truck = new Car();
                        if(truckParking.getEmptySlots() > 0) {
                            int steps = random.nextInt(10) + 1;
                            int k = truckParking.returnEmptySlot();
                            truckParking.park(truck, steps, k);
                            System.out.println("Truck with id " + id + " in " + truck.getSlot() + " slot for " + truck.getTimeForCar() + " steps");
                        }
                        else if(carParking.isEmptyForTruck() > 0){
                            int steps = random.nextInt(10) + 1;
                            int place = carParking.isEmptyForTruck();
                            carParking.parkTruckForCar(truck, steps, place);
                            carParking.parkTruckForCar(truck, steps, place+1);
                            System.out.println("Trucks with id " + id + " in " + truck.getSlot() + " and " + (truck.getSlot()+1) + " for " + truck.getTimeForCar() + " steps");
                        }
                        else{
                            System.out.println("Sorry, but there are no empty parking places for trucks");
                            System.out.println("You should wait " + truckParking.getWhenSlotsBeEmpty() + " steps until parking places are available");
                            break;
                        }
                        truckAmount--;
                        carParking.updateStatus();
                        truckParking.updateStatus();
                        id++;

                    }
                    carParking.RoundEnding();
                    truckParking.RoundEnding();
                    break;
                }
                case("status"):{
                    System.out.println("Choose the variant:");
                    System.out.println("1.car");
                    System.out.println("2.truck");
                    String type = scanner.nextLine();
                    switch(type){
                        case ("1"):{
                            int x = carParking.getParkingSlots();
                            int y = carParking.getEmptySlots();
                            System.out.println("There are " + x + "  slots in this parking");
                            System.out.println("Empty slots " + y);
                            System.out.println("Slots taken " + (x-y));
                            if (y == 0) {
                                System.out.println("There will be empty slots in " + carParking.getWhenSlotsBeEmpty() + " steps until parking places are available");
                            }
                            break;
                        }
                        case ("2"):{
                            int x = truckParking.getParkingSlots();
                            int y = truckParking.getEmptySlots();
                            System.out.println("There are " + x + "  slots in this parking");
                            System.out.println("Empty slots " + y);
                            System.out.println("Slots taken " + (x-y));
                            if (y == 0) {
                                System.out.println("There will be empty slots in " + truckParking.getWhenSlotsBeEmpty() + " steps until parking places are available");
                            }
                            break;
                        }
                    }
                    break;
                }

                case("clear"):{
                    System.out.println("Choose the variant:");
                    System.out.println("1.Clear the car parking ");
                    System.out.println("2.Clear the truck parking");
                    String type = scanner.nextLine();
                    switch(type) {
                        case ("1"): {
                            carParking.clearParking();
                            System.out.println("Car parking is clear");
                            break;
                        }
                        case ("2"): {
                            truckParking.clearParking();
                            System.out.println("Truck parking is clear");
                            break;
                        }
                    }
                    break;
                }
            }
        }
    }
}