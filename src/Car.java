public class Car {
    private int timeForCar;
    private int slot;


    public Car() {
        this.timeForCar = 0;
        this.slot = 0;
    }


    public int getSlot()  {
        return slot;
    }

    public int getTimeForCar() {
        return timeForCar;
    }


    public void setSlot(int place) {
        this.slot = place;
    }
    public void setWhereVehicleParked() {
    }

    public void setTimeForCar(int timeForCar) {
        this.timeForCar = timeForCar;
    }

}